#include <iostream>
#include <cstdlib>
#include <time.h>

int main(void)
{
   static time_t t;
   static struct tm *ltp;
   static char buffer[20];
   
   t   = time(NULL);
   ltp = localtime(&t);
   if (ltp == NULL) 
     {
	std::cout << "Localtime Function Error!" << std::endl;
	exit(EXIT_FAILURE);
     }
   
   
   if (strftime(buffer, 20, "%F %I:%M:%S", ltp) == 0) 
     {
	std::cout << "Strftime Function Error!" << std::endl;
	exit(EXIT_FAILURE);
     }
   
   std::cout << "Current Time: " << buffer << std::endl;
   return 0;
}
