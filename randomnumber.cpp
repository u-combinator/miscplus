#include <iostream>
#include <cstdlib>
#include <ctime>

int main(void)
{
    int num;
    std::srand(std::time(0));
    
    for (int i = 0; i < 10; i++) {
        num = rand() % 10;
        std::cout << "Random Number: " << num << "\n";
    }
   
    return EXIT_SUCCESS;
}