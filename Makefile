CG = g++
CL = clang++
CFLAGS = -std=c++11

all: gcc clang

gcc:
	@${CG} ${CFLAGS} -o sysinfoprint_g sysinfoprint.cpp
	@${CG} ${CFLAGS} -o currenttime_g currenttime.cpp
	@${CG} ${CFLAGS} -o cppwaystime_g cppwaystime.cpp
	@${CG} ${CFLAGS} -o randomnumber_g randomnumber.cpp
	@${CG} ${CFLAGS} -o cp11rn_g cp11rn.cpp
	@${CG} ${CFLAGS} -o imc_g imc.cpp

clang:
	@${CL} ${CFLAGS} -o sysinfoprint_c sysinfoprint.cpp
	@${CL} ${CFLAGS} -o currenttiem_c currenttime.cpp
	@${CL} ${CFLAGS} -o cppwaystime_c cppwaystime.cpp
	@${CL} ${CFLAGS} -o randomnumber_c randomnumber.cpp
	@${CL} ${CFLAGS} -o cp11rn_c cp11rn.cpp
	@${CL} ${CFLAGS} -o imc_c imc.cpp

gclean:
	@rm sysinfoprint_g
	@rm currenttime_g
	@rm cppwaystime_g
	@rm randomnumber_g
	@rm cp11rn_g
	@rm imc_g

cclean:
	@rm sysinfoprint_c
	@rm currenttime_c
	@rm cppwaystime_c
	@rm randomnumber_c
	@rm cp11rn_c
	@rm imc_c

clean:
	@rm sysinfoprint_g
	@rm sysinfoprint_c
	@rm currenttime_g
	@rm currenttime_c
	@rm cppwaystime_g
	@rm cppwaystime_c
	@rm randomnumber_g
	@rm randomnumber_c
	@rm cp11rn_g
	@rm cp11rn_c
	@rm imc_g
	@rm imc_c

.PHONY: all gcc clang gclean cclean clean
