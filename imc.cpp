#include <iostream>
#include <cstdlib>

int main(int argc, char **argv)
{
    double tall, weight;

    if (argc != 3) {
	std::cout << "Useage: " << "imc Tall(M) Weight(KG)" << std::endl;
	exit(EXIT_FAILURE);
    }

    tall   = std::atof(argv[1]);
    weight = std::atof(argv[2]);

    double IMC(weight / (tall * tall));

    std::cout << "Tall(M):    " << tall   << std::endl;
    std::cout << "Weight(KG): " << weight << std::endl;
    std::cout << "IMC:        " << IMC    << std::endl;

    return EXIT_SUCCESS;
}