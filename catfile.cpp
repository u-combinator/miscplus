#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << "Usage: catfile FILE_NAME" << std::endl;
        exit(1);
    }

    int i(1);
    std::string file_buffer;
    std::ifstream file(argv[1], std::ifstream::in);
    
    if (!file) {
        std::cerr << "Open file failed.\n";
        exit(1);
    }

    while(std::getline(file, file_buffer)) {
        std::cout << std::left << std::setw(4) << i << file_buffer << "\n";
        ++i;
    }
    
    file.close();
    return 0;
}