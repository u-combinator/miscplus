#include <iostream>
#include <cstdlib>
#include <sys/utsname.h>

int main(void)
{
    struct utsname uts;
    if (uname(&uts) == -1) {
	std::cout << "Uname function error!" << std::endl;
	exit(EXIT_FAILURE);
    }

    std::cout << "Node   Name: " << uts.nodename << std::endl;
    std::cout << "System Name: " << uts.sysname  << std::endl;
    std::cout << "Release    : " << uts.release  << std::endl;
    std::cout << "Version    : " << uts.version  << std::endl;
    std::cout << "Machine    : " << uts.machine  << std::endl;

    return EXIT_SUCCESS;
}