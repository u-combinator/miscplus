#include <iostream>
#include <iomanip>
#include <ctime>
 
int main()
{
    std::time_t t = std::time(nullptr);
    if (!t) {
        std::cerr << "Time Error!\n";
    }
    
    std::cout << "Now is: " << std::put_time(std::localtime(&t), "%F %I:%M:%S") << '\n';
    
    return 0;
}